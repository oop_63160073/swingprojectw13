/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanadoninter.swingproject;

import java.util.concurrent.ThreadLocalRandom;

/**
 *
 * @author Acer
 */
public class Computer {

    private int comHand;
    private int playerHand;
    private int win, draw, lose;

    public Computer() {
    }

    public int choob() {
        return ThreadLocalRandom.current().nextInt(0, 3);
    }

    public int paoYingChoob(int pHand) {
        this.playerHand = pHand;
        this.comHand = choob();
        
        if ((playerHand == 0 && comHand == 1)
                || (playerHand == 1 && comHand == 2)
                || (playerHand == 2 && comHand == 0)) {
            win++;
            return 1;
        }
        if (playerHand == comHand) {
            draw++;
            return 0;
        }
        lose++;
        return -1;
    }

    public int getComHand() {
        return comHand;
    }

    public int getPlayerHand() {
        return playerHand;
    }

    public int getWin() {
        return win;
    }

    public int getDraw() {
        return draw;
    }

    public int getLose() {
        return lose;
    }
    
}
